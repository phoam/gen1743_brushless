var http = require('http');
var express = require('express'),
    app = module.exports.app = express();

var server = http.createServer(app);
var io = require('socket.io').listen(server);

var bodyParser = require('body-parser')
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


// SerialPort lib
var SerialPort = require("serialport");
const Readline = SerialPort.parsers.Readline

var port = new SerialPort("/dev/ttyUSB0");
port.on('open', function(){
  console.log('Serial Port Opened');
});

const parser = port.pipe(new Readline({ delimiter: '\r\n' }))
var line = null;
parser.on('data', (data) => {
    if (data !== '') {
        line = data;
    }
});

io.on('connection', function (socket) {
    setInterval(function () {
        socket.volatile.emit('currentSpeed', line);
    }, 500);
    socket.on ('setSpeed', function (data) {
        port.write("speed set at:" + data);
        console.log(Buffer(data))
    });
});

server.listen(3000, () => console.log('Server Running'))